/** FROM https://www.kevinleary.net/javascript-get-url-parameters/
 * JavaScript Get URL Parameter
 * 
 * @param String prop The specific URL parameter you want to retreive the value for
 * @return String|Object If prop is provided a string value is returned, otherwise an object of all properties is returned
 */
function getUrlParams( prop ) {
    var params = {};
    var search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf( '?' ) + 1 ) );
    var definitions = search.split( '&' );
  
    definitions.forEach( function( val, key ) {
        var parts = val.split( '=', 2 );
        params[ parts[ 0 ] ] = parts[ 1 ];
    } );
  
    return ( prop && prop in params ) ? params[ prop ] : params;
  }

var words = [];
var counter=0;
var chosen_letters = [];
var letter_counter=0;
var letters_found=0;

function checker(argument, id) {
    if (argument==0) {
        $("#letter"+id).css('color', 'red');
    }
    else {
        letters_found++;
        $("#letter"+id).css('color', 'green');
        if (letters_found==letter_counter) {
            if (++counter==words.length){
                $( "#alx_word" ).html("<img src='img/happy_robot.gif' />");
            }
            else {
                //$("#letter"+id).css('color', 'green');
                $( "#alx_word" ).hide( "fade", {}, 500, function() {
                    next_round(words[counter]);
                });
            }
        }
    }
}

function next_round(word) {
    letter_counter=0;
    letters_found=0;
    $("#alx_word").html('');
    for (var i=0; i<word.length; i++) {
        if (jQuery.inArray( word[i], chosen_letters)<0)
            $("#alx_word").append("<span class='letter' id='letter" + i + "' onclick='checker(0, " + i + ");'>" + word[i] + "</span>");
        else {
            $("#alx_word").append("<span class='letter' id='letter" + i + "' onclick='checker(1, " + i + ");'>" + word[i] + "</span>");
            letter_counter++;
        }
    }
    $( "#alx_word" ).removeAttr( "style" ).hide().fadeIn();
}

$( document ).ready(function() {
    var game_id = getUrlParams('id');
    if ( (game_id.length>0) && (game_id!='not available') ) {
        var jqxhr = $.getJSON("activities/" + game_id + ".json", function(data) {
            $('#alx_title').html(data['title'] + ": " + data['letter']);
            $('#alx_letters_2_find').html(data['letter']);
            words=data['words'].split('|');
            chosen_letters=data['letter'].split(',');
            next_round(words[counter]);
        })
        .fail(function() {
            console.log('failed to load the game');
        });
    }
    else {
        $('#alx_title').html();
        $('#alx_letters_2_find').html();
        message = '<a href="?id=a_00">Γράμμα Β</a><br />';
        message = message + '<a href="?id=a_01">Γράμμα Γ</a><br />';
        message = message + '<a href="?id=a_02">Γράμμα Δ</a><br />';
        message = message + '<a href="?id=a_03">Γράμμα Ζ</a><br />';
        message = message + '<a href="?id=a_04">Γράμμα Θ</a><br />';
        message = message + '<a href="?id=a_05">Γράμμα Κ</a><br />';
        message = message + '<a href="?id=a_06">Γράμμα Λ</a><br />';
        message = message + '<a href="?id=a_07">Γράμμα Μ</a><br />';
        message = message + '<a href="?id=a_08">Γράμμα Ν</a><br />';
        message = message + '<a href="?id=a_09">Γράμμα Ξ</a><br />';
        message = message + '<a href="?id=a_10">Γράμμα Π</a><br />';
        message = message + '<a href="?id=a_11">Γράμμα Ρ</a><br />';
        message = message + '<a href="?id=a_12">Γράμμα Σ</a><br />';
        message = message + '<a href="?id=a_13">Γράμμα Τ</a><br />';
        message = message + '<a href="?id=a_14">Γράμμα Φ</a><br />';
        message = message + '<a href="?id=a_15">Γράμμα Χ</a><br />';
        message = message + '<a href="?id=a_16">Γράμμα Ψ</a><br />';

        $("#alx_word").html(message);
    }
});